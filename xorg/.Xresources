st.termname: st-256color
st.borderless: 0
st.font: Hack:size=9:rgba=none:antialias=true:autohint=true;
st.borderpx: 3
st.boxdraw: 1

! Available cursor values: 2 4 6 7 = █ _ | ☃️ ( 1 3 5 are blinking versions)
st.cursorshape: 2

! thickness of underline and bar cursors
st.cursorthickness: 2

! 0: normal blinking, 1: leave cursor border and blink with cursors background
st.cursorblinkstyle: 0

! 0: cursor blinks with a constant interval; 1: blinking cycle resets on key input
st.cursorblinkontype: 1

st.disablebold: 0
st.disableitalics: 0
st.disableroman: 0

! Amount of lines scrolled
st.scrollrate: 1

! Kerning / character bounding-box height multiplier
st.chscale: 1.0

! Kerning / character bounding-box width multiplier
st.cwscale: 1.0

! blinking timeout for terminal and cursor blinking (0 disables)
st.blinktimeout: 0

! bell volume. Value between -100 and 100. (0 disables)
st.bellvolume: 0


! (0|1) boxdraw(bold) enable toggle
st.boxdraw_bold: 1

! braille (U28XX):  1: render as adjacent "pixels",  0: use font
st.boxdraw_braille: 0

! set this to a non-zero value to force window depth
st.depth: 0

! opacity==255 means what terminal will be not transparent, 0 - fully transparent
! (float values in range 0 to 1.0 may also be used)
st.opacity: 0.90

! (0|1) dont pre-multiply color values of the pixels when using transparency
! (useful for the systems where composite manager not running all the time,
! as color pre-multiplication will make the "opaque" colors look darker/lighter
! without the composition)
st.disable_alpha_correction: 0

! undercurl style. either 1, 2 or 3 (0 disables)
!
! 0: Curly
!  _   _   _   _
! ( ) ( ) ( ) ( )
!  (_) (_) (_) (_)
!
! 1: Spiky
! /\  /\  /\  /\
!   \/  \/  \/
!
! 2: Capped
!  _     _     _
! / \   / \   / \
!    \_/   \_/
st.undercurl_shape: 1
! adds 1 pixel of thickness to the undercurl for every undercurl_thickness_threshold pixels of font size
st.undercurl_thickness_threshold: 28

st.foreground: #e0e0e0
st.background: #292d3e

! black
st.color0: #292d3e 
! red
st.color1: #f07178 
! green
st.color2: #c3e88d 
! yellow
st.color3: #ffcb6b 
! blue
st.color4: #82aaff 
! magenta
st.color5: #c792ea 
! cyan
st.color6: #89ddff 
! white
st.color7: #d0d0d0 
! bright black
st.color8: #434758 
! bright red
st.color9: #ff8b92 
! bright green
st.color10: #ddffa7 
! bright yellow
st.color11: #ffe585 
! bright blue
st.color12: #9cc4ff 
! bright magenta
st.color13: #e1acff 
! bright cyan
st.color14: #a3f7ff 
! bright white
st.color15: #ffffff 
! ?
st.color16: #00ff00

st.cursorColor:  #e0e0e0
st.rcs: 256
